CHAT_TEXT_FADEOUT_TIME = 5000;
// perspective of sing le user .

var widthForUserList = 400;

function GameUi(game) {
	
	this.chatBoxDivList = [];
	this.game = game;
	this.userIdDivMap = {};
	this.playerMafiaKillChoices={};
	this.playerUserKillChoices={};
	this.playerPoliceChoice="";
	this.playerMedicChoice="";
	this.userMafiaChoice=[];
	this.previousDayFraction = 0;//day . night
	this.previousNightFraction = 0;//day . night
	
	this.setLoadingScreen = function(message,timeInSeconds){
		var a = $('#loading').meerkat({
			background: '#f7fcfa url(./images/bg-splash.png) repeat-x left top',
			height: '100%',
			width: '100%',
			position: 'bottom',
			animationIn: 'none',
			animationOut: 'fade',
			animationSpeed: 500,
		});
		$("#loadingScreenText").html(message);
		if(timeInSeconds){
			setTimeout(this.removeLoadingScreen,timeInSeconds*1000);
		}
	}
	this.removeLoadingScreen = function(){
		$('#loading').destroyMeerkat();
	}
	
	this.getElement = function(idStr) {
		return $(idStr);// document.getElementById(idStr);
	}
	this.getChatTextElement = function(msg, picUrl, time, self) {
		var msg = msg.split("\n");
		var msgStr = "";
		for (var i = 0; i < msg.length; i++) {
			msgStr += "<p>" + msg[i] + "</p>";
		}
		return $('<li class="' + (self ? "self" : "other") + '">'
				+ '<div class="avatar">' + '<img src="' + picUrl + '" />'
				+ '</div>' + '<div class="messages">' + msgStr
				+ '<time datetime="' + time + '">' + time + '</time>'
				+ '</div>' + '</li>');
	}

	this.addMessageChatBoxList = function(user, msg) {
		var chatBoxDivList = this.chatBoxDivList;
		if (chatBoxDivList.length > MAX_MESSAGES_IN_CHAT_LIST) {
			$(chatBoxDivList.shift()).fadeOut(CHAT_TEXT_FADEOUT_TIME);
		}
		console.log(user, game);
		console.log(msg, user.pic, (new Date()).toLocaleTimeString(), false);
		var div = this.getChatTextElement(msg, user.pic,
				(new Date()).toLocaleTimeString(),
				this.game.currentUserId == user.userId ? true : false)
				.appendTo($("#discussionBox"));
		//console.log(div);
		chatBoxDivList.push(div);
	}
	this.hideVoteButton = function(){
		$("#voteButton").hide();
	}
	this.showVoteButton = function(){
		$("#voteButton").show();
	}
	
	this.clearChatBox = function() {
			while(this.chatBoxDivList.length>0){
				$(chatBoxDivList.shift()).hide();
			}
	}

	this.setChatBoxTitle = function(heading) {
		$("#chatBoxTitle").html(heading);	
	}

	this.slideChatBoxOut = function() {
		 $("#chatModule").stop().animate({right: -500+'px'}, 800);
	}

	this.slideChatBoxIn = function() {
		 $("#chatModule").stop().animate({right: 0+'px'}, 800);
	}

	this.sendChatTextButtonClick = function(text) {
		// wait for 10 seonds -------TODO to send another message
		this.game.sendMessageToServer(TEXT_COMMUNICATION,text);
	}
	
	this.phaseChangeAnimation = function(gameStatus) {
		 //var MAFIA_ACTIVE,POLICE_ACTIVE,MEDIC_ACTIVE,VOTING_ACTIVE,ROUND_RESULTS;
		//this.game.timeLineDuration  =  [MAFIA_ACTIVE_TIME,POLICE_ACTIVE_TIME,MEDIC_ACTIVE_TIME,VOTING_ACTIVE_TIME,ROUND_RESULTS_TIME]; // seconds
		//this.game.timeLineStates  =  [MAFIA_ACTIVE,POLICE_ACTIVE,MEDIC_ACTIVE,VOTING_ACTIVE,ROUND_RESULTS]; // seconds
		//this.timeLineDayfractionMap =     [0.3 , 0.6, 1, 0.5 ,0.5 ];
		//this.timeLineDayStatesMap =     [NIGHT , NIGHT,NIGHT,DAY,DAY];
		
		var timeElapsed = 0;
		
		for(var i=0;i<this.game.timeLineStates.length;i++){
			if(gameStatus == this.game.timeLineStates[i]) break;
		}
		var fractionElapsed = this.game.timeLineDayfractionMap[i]; // fraction of day/night elapsed
		var time = this.game.timeLineDurations[i]; // 
		var dayState = this.game.timeLineDayStatesMap[i]; // day or night 
		this.backgroundAnimations(dayState,fractionElapsed, time);
		//TODO , clean code below
		this.slideChatBoxOut();
		this.clearAllVotes();
		this.hideVoteButton();
		switch(gameStatus){
				case MAFIA_ACTIVE:
					this.slideChatBoxIn();
					this.setChatBoxTitle("Chat with mafias");
					break;
				case POLICE_ACTIVE:
					break;
				case MEDIC_ACTIVE:
					break;
				case VOTING_ACTIVE:
					this.slideChatBoxIn();
					this.setChatBoxTitle("Chat with villagers");
					break;	
				case ROUND_RESULTS:
					break;
			}
	}
	
	this.finalizePoliceIdentifyOkButtonOnClick = function(){
		this.game.hasPoliceIdentified = true;
		this.game.sendMessageToServer(POLICE_IDENTIFY,this.playerPoliceChoice);
	}
	this.finaliseUserKillOkButtonClick = function() {
		this.game.sendMessageToServer(KILL_USER_VOTE,this.playerUserKillChoices[this.game.currentUser]);
	}
	
	this.finaliseMafiaKillOkButtonClick = function() {
		this.game.sendMessageToServer(MAFIA_KILL,this.playerMafiaKillChoices[this.game.currentUser]);
	};
	this.showUserVotesOnUi = function(userId1, userId2){
		//var userList = this.game.users;
		this.clearAlluserVotesOnUi();
		this.playerUserKillChoices[userId1] = userId2;
		var keys = Object.keys(this.playerUserKillChoices);
		for(var i=0;i<keys.length;i++){
				var choiceUser = this.playerUserKillChoices[keys[i]];
				var userObj= this.game.getUserById(choiceUser);
				var text = this.userIdDivMap[choiceUser].find(".userKillList").html();
				// if it current do some highlighting
				this.userIdDivMap[choiceUser].find(".userKillList").html(text+","+userObj.name);
		}
	}
	
	this.clearAllVotes = function(){
		this.clearAllMafiaChoiceListOnUi();
		this.clearAlluserVotesOnUi();
	}
	
	this.clearAlluserVotesOnUi = function(){
		for(var i=0; i<this.game.users.length;i++ ){
			this.userIdDivMap[this.game.users[i].userId].find(".userKillList").html("");
		}
	}
	this.clearAllMafiaChoiceListOnUi = function(){
		for(var i=0; i<this.game.users.length;i++ ){
			this.userIdDivMap[this.game.users[i].userId].find(".mafiaKillList").html("");
		}
	}
	this.showMafiaVotesFromUsersOnUi = function(userId, userListIds){
		this.clearAllMafiaChoiceListOnUi(); //optimise here, mostly not needed
		this.playerMafiaKillChoices[userId] = userListIds;
		console.log(userId,userListIds,this.playerMafiaKillChoices);
		var temp= {}
		var keys = Object.keys(this.playerMafiaKillChoices);
		for(var i=0;i<keys.length;i++){
			var choiceList = this.playerMafiaKillChoices[keys[i]];
			for(var j=0;j<choiceList.length;j++){
				//'<div class="mafiaKillList"></div>'+
				//var userObj= this.game.getUserById(choiceList[j]);
				var text = this.userIdDivMap[choiceList[j]].find(".mafiaKillList").html();
				// if it current do some highlighting
				this.userIdDivMap[choiceList[j]].find(".mafiaKillList").html(text+","+this.game.getUserById(userId).name);
			}
		}
	};
	
	this.onUserSelect = function(userId) {
		var user = this.game.getUserById(userId);
		var player = this.game.getUserById(this.game.currentUser);
		var gameStatus =this.game.getCurrentState();
		switch(gameStatus){
			case MAFIA_ACTIVE:
				// make voting selection
				console.log(player.isMafia());
				if(!player.isMafia())
					break;
				var usermafiaChoices = this.playerMafiaKillChoices[player.userId]?this.playerMafiaKillChoices[player.userId]:[];
				var index = usermafiaChoices.indexOf(userId);
				if(index > -1)
					usermafiaChoices.splice(index,1);
				usermafiaChoices.push(userId);
				
				while(usermafiaChoices.length > 3){
					usermafiaChoices.shift();
				}
				this.showMafiaVotesFromUsersOnUi(this.game.currentUser,usermafiaChoices);
				this.showVoteButton();
				break;
				
			case POLICE_ACTIVE:
				if(!player.isPolice())
					break;
				$(".policeIdentity").removeClass(".policeIdentity");
				this.userIdDivMap[userId].addClass(".policeIdentity");
				this.playerPoliceChoice=userId;
				this.showVoteButton();
				break;
			case MEDIC_ACTIVE:
				// make voting selection
				if(!player.isMedic())
					break;
				$(".medicSave").removeClass(".medicSave");
				this.userIdDivMap[userId].addClass(".medicSave");
				this.playerMedicChoice=userId;
				this.showVoteButton();
				break;
			case VOTING_ACTIVE:
				// make voting selection
				this.showUserVotesOnUi(player.userId, userId);
				this.showVoteButton();
				break;
			case ROUND_RESULTS:
				break;
		}
				
	}
	this.backgroundAnimations= function(gameDayStatus,x,time){
		console.log(gameDayStatus,x,time);
		if(gameDayStatus==DAY){
			this.dayPhase(x,time);
		}
		else if(gameDayStatus==NIGHT){
			this.nightPhase(x,time);
		}
	}
	
	this.dayPhase =function(x,time){  // x is the fraction of day from sunrise to sunset , sunrise =0  , sunset =1
		//TODO , animation to max top and come down even if the fraction jumps over 0.5
		var temp=0;
		if(this.previousDayFraction <0.5 && x> 0.5){
			var copyX = x;
			temp = this.previousDayFraction;
			var nextPartOfAnimationTime= ((copyX-0.5)/(copyX-temp))*time;
			x =0.5;
			time = ((0.5-temp)/(x-temp))*time;
			setTimeout(function(){ 
				this.dayPhase(copyX,nextPartOfAnimationTime); 
				}  , time);
		}
		
		this.previousDayFraction =x;
		temp =(x-0.5)*(x-0.5);
		$('#sun_yellow').animate({'top':(30+(96-30)*4*temp)+'%','opacity':1-temp}, time);
		$('#sun_red').animate({'top':(30+(96-30)*4*temp)+'%','opacity':temp}, time);
		$('#sky').animate({'backgroundColor':(x>0.5 ?jQuery.intermediateColor("#FFFFFF",'#4F0030',x-0.5):'#FFFFFF') }, time);
//		$('#clouds').animate({'backgroundPosition':'1000px 0px','opacity':0}, 30000);
		$('#stars').animate({'opacity':x>0.5?x-0.5:0 }, time);
		$('#night').animate({'opacity':x>0.5?x-0.5:0 }, time);
	} 
	this.nightPhase =function(x,time){
		//// just to make sure it animated to 0.5 max and domes down without skipping
		var temp=0;
		if(this.previousNightFraction <0.5 && x> 0.5){
			var copyX = x;
			temp = this.previousNightFraction;
			var nextPartOfAnimationTime= ((copyX-0.5)/(copyX-temp))*time;
			x =0.5;
			time = ((0.5-temp)/(x-temp))*time;
			setTimeout(function(){ 
				this.nightPhase(copyX,nextPartOfAnimationTime); 
				}  , time);
		}
		
		$('#moon').animate({'top':(30+(96-30)*4*(x-0.5)*(x-0.5))+'%','opacity':0.8}, time);
		temp =(0.25 - (x-0.5)*(x-0.5));
		$('#sky').animate({'backgroundColor':jQuery.calculateInBetweenColor("#FFFFFF",'#4F0030',0.5+2*temp) }, time);
//		$('#clouds').animate({'backgroundPosition':'1000px 0px','opacity':0}, 30000);
		$('#stars').animate({'opacity':(0.5+2*temp) }, time);
		$('#night').animate({'opacity':0.8*4*temp}, time);
		this.previousNightFraction =x;
	} 

	this.displayUsersOnScreen = function(userList) {
		var countInRow  = userList.length/3; //configure rows 3 also in fonfig
		countInRow= countInRow<3?3:countInRow;//ensure minimum three
		var widthOfDisplayPic = widthForUserList/countInRow;
		
		
		var thisRef=this;
		var ul = $('<ul>').addClass('polaroids');
		for(var i=0;i< userList.length ;i++){
			var userDiv = $('<li>'+
					'<a href="#" title="'+userList[i].name+'">'+
					'<img alt="'+userList[i].name+'" src="'+userList[i].pic+'" style="width:'+widthOfDisplayPic+'px;"/>'+
					'<div class="mafiaKillList"></div>'+
					'<div class="userKillList"></div>'+
					'</a>'+
					
				'</li>').appendTo(ul);
			userDiv.click(function(userId){ 
				return function(){    
				 thisRef.onUserSelect(userId);
					}
				} (userList[i].userId)); // create a binding function
			
			this.userIdDivMap[userList[i].userId] = userDiv;
			
		}
		ul.appendTo("#userListContainer");
	}
	
	this.animateClouds = function(clouds1 , clouds2){
		var temp = this;
		$(clouds1).animate({'left':'100%','opacity':'1'}, 30000,function(){
			$(clouds1).css({"left":"-100%"});
		});
		$(clouds2).animate({'left':'0%','opacity':'1'}, 30000,function(){
			temp.animateClouds(clouds2,clouds1);
		});
	};
}

//$(document).ready();
