var gameSettings = {
	canvasId  : "game_canvas",
	width     : 500,
	height    : 300,
};

var resources = {
	
};
if(typeof zxgame === "undefined" || !zxgame) {
	"use strict";
	window.zxgame = (function(){
		var _zxgame = function(settings) {
			this.settings = settings;
			this.canvas = "";
		};
		
		_zxgame.prototype.setup = function() {
			this.canvas = document.getElementById(this.settings.canvasId);
			this.canvas.width = this.settings.width;
			this.canvas.height = this.settings.height;
		};
		return _zxgame;
	})();
}


// game logic start here
var game = new zxgame(gameSettings);