DAY = 0;
NIGHT =1 ;

WAITING_FOR_PLAYERS = 2;
EVERYONE_SLEEP = 3;
MAFIA_ACTIVE =4;
POLICE_ACTIVE=5;
MEDIC_ACTIVE=6;
DEBATE_ACTIVE = 7 ;
VOTING_ACTIVE = 8;
ROUND_RESULTS = 31;
RANDOMLY_PAIR_USERS =9;
START_A_PRIVATE_SESSION = 10;
IS_DEAD = 11;
IS_ALIVE  = 12 ;
IS_MAFIA = 13 ;
IS_VILLAGER = 14;
IS_POLICE = 15;
IS_MEDIC  =16;
IS_CONNECTION_ACTIVE = 17;
IS_SESSSION_ACTIVE =18;

INIT_USERS_LIST= 19;
USER_TYPE = 35;

GAME_STATE = 20;
TIMELINE_STATE=21;
TEXT_COMMUNICATION = 22;
TEXT_COMUNICATION_FROM = 33;
MAFIA_KILL =23;
POLICE_IDENTIFY = 24;
POLICE_IDENTIFY_RESULT =25;
MEDIC_SAVE  = 26;
KILLED_BY_MAFIA = 27;
KILL_USER_VOTE = 28;

KILLED_BY_USER_VOTING = 32;
TIME_LINE_DURATIONS =34

MAX_MESSAGES_IN_CHAT_LIST = 3;
INITIAL_SLEEP_TIME=60;
MAFIA_ACTIVE_TIME = 180;
POLICE_ACTIVE_TIME = 60;
MEDIC_ACTIVE_TIME = 60;
VOTING_ACTIVE_TIME = 240;
ROUND_RESULTS_TIME = 30;

getUserTypeInText = function(type){
	switch(type){
	case IS_MAFIA:
		return "Mafia";
	case IS_MEDIC:
		return "Medic";
	case IS_VILLAGER:
		return "Villager";
	case IS_POLICE:
		return "Police";
	}
	
	return "";
}

PLAYING_ROLE_TEXT = function(type){
	return "You play the role of <span class='highlightRedText'>"+getUserTypeInText(type)+"</span>";
}


function parseJson(data){
	return JSON && JSON.parse(json) || $.parseJSON(json);
}
function isStringObj(myVar){
	if (typeof myVar == 'string' || myVar instanceof String)
		return true;
	return false;
}

function User(userId, name, profilePic,lifeState){
	this.userId = userId;
	this.pic = profilePic; 
	this.lifeState = lifeState;
	this.name = name;
	this.type=null;
	
	this.getLifeState = function(){
		return this.lifeState;
	};
	this.setLifeState = function(state){
		this.lifeState = state;
	};
	this.getUserType= function(){
		return this.type;
	}
	this.setUserType = function(type){
		return this.type = type;
	}
	
	this.isPolice = function(){
		return this.type == IS_POLICE;
	}
	
	this.isMedic = function(){
		return this.type == IS_MEDIC;
	}
	this.isMafia = function(){
		return this.type == IS_MAFIA;
	}	
	this.isVillager = function(){
		return this.isPolice() || this.isMedic() || (this.type == IS_VILLAGER);
	}	
	
}

function MafiaGame(sessionId, userId){

	this.server = null;
	this.currentUser = userId;
	this.isGameValid = true;
	this.timelineState  =   DAY;
	this.currentState  =  WAITING_FOR_PLAYERS;
	this.sessionStatus  =  IS_SESSSION_ACTIVE;
	this.users  =  [];
	this.usersMap  =  {};
	this.policeUser  =  null;
	this.medicUser  =  null;
	this.mafiaList  =  [];
	this.villagersList  =  [];
	this.sessionId  =  sessionId;
	this.mafiaKillChoiceList  =  {};// user wants to kill [a , b ,c ]
	this.startTime  =  null;
	this.userKillChoiceList  =  {};
	this.hasPoliceIdentified  =  false;
	this.hasMedicSavedAnUser  =  false;
	this.timeLineDurations  =  [MAFIA_ACTIVE_TIME,POLICE_ACTIVE_TIME,MEDIC_ACTIVE_TIME,VOTING_ACTIVE_TIME,ROUND_RESULTS_TIME]; // seconds
	this.timeLineStates  =  [MAFIA_ACTIVE,POLICE_ACTIVE,MEDIC_ACTIVE,VOTING_ACTIVE,ROUND_RESULTS]; // seconds
	this.timeLineDayStatesMap =     [NIGHT , NIGHT,NIGHT,DAY,DAY];
	this.timeLineDayfractionMap =     [0.3 , 0.6, 1, 0.8 ,0.5 ];
	this.stateToFuncitonMap  =  {};
	this.roundTime  =  0;
	this.dayTimeSum = 0;
	this.nightTimeSum = 0;
	this.gameUi = new GameUi(this);

	
	this._classLoaded = function(game){
		game.gameUi.setLoadingScreen("Loading Players");
		return true;
	}(this);
	
	this.init  =  function(userList,timeLineDurations){
		for(var i=0 ;i<userList.length;i++){
			var user = new User(userList[i][0],userList[i][1],userList[i][2] ,userList[i][3]);
			this.users.push(user);
			this.usersMap[user.userId]=user;
		}
		this.timeLineDurations = timeLineDurations;
		//TODO IMP: compute the time time fractions separately for day and night
		for(var i=0;i<timeLineDurations.length;i++){
			this.roundTime+=i;
			if(this.timeLineDayStatesMap[i]==DAY) this.dayTimeSum+=i;
			if(this.timeLineDayStatesMap[i]==NIGHT) this.nightTimeSum+=i;
		}
		this.stateToFuncitonMap = {}
		this.stateToFuncitonMap[MAFIA_ACTIVE] = this.askMafiaToKillSomeOne;
		this.stateToFuncitonMap[POLICE_ACTIVE] = this.askPoliceToIdentify;
		this.stateToFuncitonMap[MEDIC_ACTIVE] = this.askMedicToSave;
		this.stateToFuncitonMap[VOTING_ACTIVE] = this.startVotingTime;
		this.stateToFuncitonMap[ROUND_RESULTS] =this.roundResultsFinalization;
		//TODO uncomment below Line
		//this.startConnectionToServer('http://localhost/game/'+sessionId+"/"+userId);
		this.sleepWithEveryone();// sleep start
		this.gameUi.displayUsersOnScreen(this.users);
		this.gameUi.removeLoadingScreen();
	};

	this.startConnectionToServer  =  function(url){
		// url = SERVER_SOCKET_URL;
		this.server = new SockJS('http://localhost/game/'+this.sessionId+"/"+this.userId , null , {debug:true});
		var game = this;
		server.onopen = function() {

		};
		server.onmessage = function(e) {
			game.processMessage(e.data);
		};
		server.onclose = function() {

		};
	};

	this.getSessionId =  function(){
		return this.sessionId ;
	};

	this.gameTrigger =  function(state){
		this.currentState = state;
		this.stateToFuncitonMap[state]();
	};

	this.getCurrentState =  function(){
		return this.currentState;
	};
	this.setCurrentState = function(state){
		this.currentState = state;
	}

	this.getSessionid =  function(){
		return this.sessionId;
	};

	this.getUserById =  function(userId){
		return this.usersMap[userId];
	};

	this.sleepWithEveryone =  function(){
		this.currentState = EVERYONE_SLEEP;
	};

	this.askMafiaToKillSomeOne =  function(){
		this.currentState = MAFIA_ACTIVE;
	};

	this.askPoliceToIdentify =  function(){
		self.currentState = POLICE_ACTIVE;
	};

	this.askMedicToSave = function(){
		self.currentState = MEDIC_ACTIVE;
	};

	this.userDebateAndVotingBegin = function(){
		this.currentState = VOTING_ACTIVE;
	};

	this.roundResultsFinalization = function(self){
		self.currentState = ROUND_RESULTS;
	};

	this.serverMessagePayloadDecoder  =  function(msg){//message will always be an array of objects , each object is a map of key:value
		if (isStringObj(msg)){
			msg = JSON && JSON.parse(msg) || $.parseJSON(msg);
		}
		var ret = [];
		if(!(msg instanceof Array)){ //if single object
			msg=[msg];
		}
		for(var a=0;a<msg.length;a++){
				for(var key in msg[a]){
					message = msg[a][key];
					ret.push([key,message]);
				}
		}
		for(var i=0;i<ret.length;i++){
			for(var j=0;j<ret.length-1;j++){
				var temp = ret[j];
				if(ret[j][0] > ret[j+1][0]){
					ret[j]=ret[i];
					ret[i]=temp;
				}
					
			}
		}

		//sorting on key number , we need to order the keys based on priority
		return ret;
	};

	this.messageDecoderWithUserId  =  function(message){
		var x = message.indexOf(":");
		message=message+'';
		console.log(message);
		try {
			return [message.substring(0,x),parseJson(message.substring(x+1))];
		}
		catch (e) {
			return [message.substring(0,x),message.substring(x+1)];
		}
	};
	
	this.animateSplash = function(msg){
	};
	
	this.finalizeUserMafiaKillList = function(key, message){
	};
	
	
	this.sendMessageToServer  =  function(key,message){
		var user = this.getUserById(this.currentuser);
		if(user.getLifeState() != IS_ALIVE) {
			return;
		}
		var temp = new Object();
		temp[key]=message
		data = jQuery.toJson(temp);
		server.send(data);
	};
	

	this.processServerMessages  =  function ( msg){
		var user = this.getUserById(this.currentuser);
		var temp=null;
		var decodedMessage =this.serverMessagePayloadDecoder(msg);
		//console.log(decodedMessage);
		for(var i = 0; i < decodedMessage.length; i++) {
			var key = decodedMessage[i][0], message= decodedMessage[i][1];
//			console.log(key,message);

			
			// replace with switch

			if(key == TEXT_COMMUNICATION && 
					(true ||this.getCurrentState() == MAFIA_ACTIVE || this.getCurrentState() == VOTING_ACTIVE )) {
				temp = this.messageDecoderWithUserId(message);
				//console.log(temp);
				fromUser = this.getUserById(temp[0]);
				message = temp[1];
				this.gameUi.addMessageChatBoxList(fromUser, message);
				// add messages to chat
				// self.broadcastMessageToUserList(user,
				// messagEncodeWithUserId(TEXT_COMUNICATION_FROM,userId,message));
				// //msg will already have userId from whomits sent
			}
			else if(key == USER_TYPE){
				this.getUserById(this.currentUser).setUserType(message);
				this.gameUi.setLoadingScreen(PLAYING_ROLE_TEXT(message),3);				
			}
			else if(key == GAME_STATE){
				var gameState = message;
				this.setCurrentState(gameState);
				/// process the statechange , as per gameState variable;
				this.gameUi.phaseChangeAnimation(gameState);
			}	
			else if(key == MAFIA_KILL &&  (this.getCurrentState() == MAFIA_ACTIVE)) {
				if(user.type == IS_MAFIA){
					temp = this.messageDecoderWithUserId(message);
					fromUser = getUserById(temp[0]);
					message = temp[1]; // list of userid choices
					// show ui change that mafia user voted to kill lthese
					// people

					// self.broadCastMessageToMafiaList(user,messagEncodeWithUserId(MAFIA_KILL,userId,message));
					// self.mafiaKillChoices(user , loads(message));
				}
			}
			
			else if(key == KILL_USER_VOTE){
				temp = this.messageDecoderWithUserId(message);
				fromUser = getUserById(temp[0]);
				message = temp[1]; // list of userid choices
				// show ui changes that user1 wants to kill user 2
				// self.userKillChoice(userId, userIdToKill);
				// self.broadcastRawMessageToList(user,
				// messagEncodeWithUserId(KILL_USER_VOTE , userId,message));
			}			
			
			else if (key == KILLED_BY_MAFIA){
				temp = this.messageDecoderWithUserId(message);
				var killedUser = getUserById(temp[0]);
				var type = temp[1]; // list of userid choices
				//self.broadCastMessageToListAppend(messagEncodeWithUserId(KILLED_BY_MAFIA,userKilledInThisRound,user.type))
			}
			
			else if(key == KILLED_BY_USER_VOTING){
				temp = this.messageDecoderWithUserId(message);
				var killedUser = getUserById(temp[0]);
				var type  = temp[1]; // list of userid choices
			}
			
			else if(key == INIT_USERS_LIST){
				//console.log(key, message);
				this.init(message[0],message[1]);
			}
		}
	}
}



