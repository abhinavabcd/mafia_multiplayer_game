from yajl import loads , dumps

def appendUserIdInfrontOfPayloadData(userId , msg):
    if(type(msg) == str):
        msg= loads(msg)
    for key in msg.keys():
        #data = msg[key].split(":")
        #messageData = ''.join(msg[1:])
        msg[key] = userId+":"+msg[key]
    
        
def messageDecoder(msg):
    if(type(msg) == str):
        msg= loads(msg)
    returnList =[]
    for key in msg.keys():
        #data = msg[key].split(":")
        #messageData = ''.join(msg[1:])
        messageData = msg[key]
        returnList.append([key,messageData])
    return returnList


def messageEncoder( key, messageData):
    return dumps({key:messageData})

def messagEncodeWithUserId(key,userId, messageData):
    return dumps({key:userId+":"+str(messageData)})

def encodeMultipleMesages():
    pass
