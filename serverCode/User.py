

from Config import *
import random
import string
from yajl import loads , dumps
 



class User():
    userId= ''
    connectionStatus = None
    name=""
    activeSession = None
    lifeState = IS_ALIVE 
    type = None
    votesAgainstUser = 0
    userMessages = []
    failedToSendMessages = []
    userConnections = []
    removeObjFromMemory = False
    connection = None
    connections={}
    pic=""
    
    def __init__(self,userId,activeSession,failedToSendMessages,lifeState,name,pic):
        self.userId= userId
        self.activeSession = activeSession
        self.failedToSendMessages = failedToSendMessages
        self.lifeState = lifeState
        self.name = name
        self.pic=pic
    
    def startANewGameSession(self,sessionId):
        self.activeSession = sessionId
        self.failedToSendMessages = []
        self.lifeState = IS_ALIVE
     
        
    def appendMessageToUser(self,message):
        self.failedToSendMessages.append(message)
        
    def sendMessageToUser(self,message=None):
        #messagesToWaitToSend flag will help accumulate message to send in bulk 
        if(message):
            self.appendMessageToUser(message)
        if(len(self.failedToSendMessages)==0):
            return
        try:
            if(self.connectionStatus == IS_CONNECTION_ACTIVE):
                self.connection.send(json.loads(self.failedToSendMessages))
                self.failedToSeSendMessages = []
        except:
            pass

    def setLifeState(self,state):
        if(state==IS_ALIVE or state==IS_DEAD):
            self.lifeState = state        
    
    def getLifeState(self):
        return self.lifeState
    
    def getUserType(self,func=int):
        return func(self.type)
    
    def setConnection(self,conn):
        self.connection = conn
    
    def isPolice(self):
        return self.type==IS_POLICE
    
    def isMedic(self):
        return self.type==IS_MEDIC
    
    def isMafia(self):
        return self.type==IS_MAFIA
    
    def saveConnection(self,conn):
        self.connections[conn]=True
        
    def destroyConnection(self,conn):
        del self.connections[conn]
        
        

    def __str__(self):
        return dumps({})# to save in mongo db
    
        #load from mongo and save to memory