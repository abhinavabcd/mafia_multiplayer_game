
from Config import *
import random
import string
from yajl import loads , dumps
import time
 
from Utilities import *

from chardet.test import count
from django.contrib.sites.managers import CurrentSiteManager

def sendGameStateToUsers(func):
    def wrapperAroundGameStateChanges(self):
        func(self)
        self.sendGameStateToUsers()
    return wrapperAroundGameStateChanges


class MafiaGame():
    isGameValid = True
    timelineState =  DAY
    currentState = WAITING_FOR_PLAYERS
    sessionStatus = IS_SESSSION_ACTIVE
    users= []
    usersMap = {}
    
    policeUser = None
    medicUser = None
    mafiaList = []
    villagersList= []
    sessionId = ''.join(random.choice(string.ascii_lowercase + string.digits) for x in range(15))
    userBroadCastList = []
    userKillVotesMap = {}
    mafiaKillChoiceList = {} #user wants to kill [a , b ,c ]
    startTime = None
    userKillChoiceList = {}
    hasPoliceIdentified = False
    hasMedicSavedAnUser = False
    timeLineDuration = [MAFIA_ACTIVE_TIME,POLICE_ACTIVE_TIME,MEDIC_ACTIVE_TIME,VOTING_ACTIVE_TIME,ROUND_RESULTS_TIME]#seconds
    timeLineStates = [MAFIA_ACTIVE,POLICE_ACTIVE,MEDIC_ACTIVE,VOTING_ACTIVE,ROUND_RESULTS]#seconds
    # while round results , annource results = evening phase , and after active night scene and sleep then follows mafiaactive
    stateToFuncitonMap = {}
     
    roundTime = 0
    
    def __init__(self,userList):
        
        if(len(userList)<6):
            return
        self.users = userList
        for i in userList:
            self.usersMap[i.userId] = i
        #initialization phase
        self.assignRolesRandomly()
        for i in self.timeLineDuration:
            self.roundTime+=i
         
        self.stateToFuncitonMap = {
                                   MAFIA_ACTIVE : self.askMafiaToKillSomeOne,
                                   POLICE_ACTIVE: self.askPoliceToIdentify , 
                                   MEDIC_ACTIVE : self.askMedicToSave,
                                   VOTING_ACTIVE: self.startVotingTime,
                                   ROUND_RESULTS: self.roundResultsFinalization
                                   }
        self.sleepWithEveryone()
        self.startTime = int(time.time())+INITIAL_SLEEP_TIME
        self.triggerAgainIn(INITIAL_SLEEP_TIME)  
        
  
    def triggerAgainIn(self,seconds):
        #time.sleep(seconds)
        self.gameTrigger()
        return
    
    def getSessionId(self):
        return self.sessionId       
            
    def gameTrigger(self):# make sure this is not recursise , with different thread callbacks
        state , timeForNextState = self.getCurrentState()
        self.triggerAgainIn(timeForNextState)
        
        if(self.currentState!=state):
            self.stateToFuncitonMap[state]() # state change 
        self.currentState = state
        
              
    def getCurrentState(self):
        secondsElapsed = (self.startTime - int(time.time()))%self.roundTime
        count=0
        while(count < len(self.timeLineDuration)):
            secondsElapsed -= self.timeLineDuration[count]
            if(secondsElapsed< 0):
                return self.timeLineStates[count],-secondsElapsed
            count+=1
        
    def getSessionid(self):
        return self.sessionId
    
    def getAllMafia(self):
        return self.mafiaList
    
    def getAllVillagers (self):
        return self.villagersList
    
    def getPoliceUser(self):
        return self.policeUser
    
    def getMedicUser(self):
        return self.medicUser
    
    def getUserById(self,userId):
        self.usersMap.get(userId,None)
        
    #sun rise phase
    def assignRolesRandomly(self):
        self.users.shuffle()
        mafiaCount = (len(self.users)+1)/3
        for user in self.users[0:mafiaCount]:
            user.type = IS_MAFIA
        
        self.mafiaList = self.users[0:mafiaCount]
        
        self.users[mafiaCount]= IS_POLICE
        self.users[mafiaCount+1]=IS_MEDIC
        for user in self.users[mafiaCount+2:]:
            user.type = IS_VILLAGER
            
        self.villagersList = self.users[mafiaCount:]
        #tell users
        for user in self.users:
            user.sendMessageToUser(dumps({USER_TYPE:user.type,
                                          INIT_USERS_LIST: [
                                                            [ [x.userId, x.name,x.pic, x.getLifeState] for x in self.usersMap.values() ],
                                                            self.timeLineDuration 
                                                            #TODO , not good to send them in the same list , 
                                                            #send seperately and change appropriately in 
                                                            #js
                                                            ]
                                          }))
        
       
    #night Phase
    def sendTimelineState(self):
        for user in self.users:
            user.sendMessageToUser(dumps({TIMELINE_STATE:self.timelineState}))
    
    def sendGameStateToUsers(self):
        for user in self.users:
            user.sendMessageToUser(dumps({GAME_STATE:self.currentState}))
            
    @sendGameStateToUsers
    def sleepWithEveryone(self):# :P
        self.currentState = EVERYONE_SLEEP
        return
    
    # 2 minutes
    @sendGameStateToUsers
    def askMafiaToKillSomeOne(self):
        self.currentState = MAFIA_ACTIVE
        self.userBroadCastList = self.getAllMafia()
        self.mafiaKillChoiceList= {}
        
    # 1 minute   
    @sendGameStateToUsers
    def askPoliceToIdentify(self):
        self.hasPoliceIdentified = False
        self.currentState = POLICE_ACTIVE
        self.userBroadCastList =[]
        
    #1 minute
    @sendGameStateToUsers
    def askMedicToSave(self):
        self.hasMedicSavedAnUser = False
        self.currentState = MEDIC_ACTIVE
        self.userBroadCastList =[]
    
    @sendGameStateToUsers
    def userDebateAndVotingBegin(self):
        self.hasMedicSavedAnUser = False
        self.currentState = VOTING_ACTIVE
        self.userBroadCastList = self.users
 
    @sendGameStateToUsers
    def roundResultsFinalization(self):
        self.currentState = ROUND_RESULTS
        self.userBroadCastList =self.users
        self.finalizeUserKillPhase()
        
    
         
#     @sendGameStateToUsers
#     def startDebate(self):
#         self.currentState = DEBATE_ACTIVE
#         self.userBroadCastList = self.users
        
    @sendGameStateToUsers
    def startVotingTime(self):
        self.currentState = VOTING_ACTIVE
        self.userBroadCastList = self.users
        self.userKillChoiceList= []
    
    
    def finalizeMafiaKillPhase(self):
        maxVotes = 0 
        toKill = self.getAnAliveUser().userId
        tempMap ={}
        for i in self.users:
            tempMap[i]=0
            
        for userIdList in self.mafiaKillChoiceList.values():
            for userId in userIdList:
                tempMap[userId]+=1
        for userId in tempMap.keys():
            if(maxVotes <= tempMap[i]):
                maxVotes = tempMap[userId]
                toKill = userId
        
        self.getUserById(toKill).setLifeState(IS_DEAD)
    
    def finalizeUserKillPhase(self):
        maxVotes = 0 
        toKill = self.getAnAliveUser().userId
        tempMap ={}
        for i in self.users:
            tempMap[i]=0
                
        for user in self.userKillChoiceList.values():
            tempMap[user.userId]+=1
        
        for userId in tempMap.keys():
            if(maxVotes <= tempMap[i]):
                maxVotes = tempMap[userId]
                toKill = userId
                
        self.getUserById(toKill).setLifeState(IS_DEAD)
        self.broadCastMessageToListAppend(messagEncodeWithUserId(KILLED_BY_USER_VOTING,toKill,self.getUserById(toKill).getUserType(str) ))

    
    def broadCastMessageToListWithUserId(self,fromUser,key, messageData):
        for u in self.users:
            if(u.userId==fromUser.userId): continue
            u.sendMessageToUser(messagEncodeWithUserId(key, fromUser.userId,messageData))
     
    def broadCastMessageToListAppend(self,fromUser,key,messageData):
        for u in self.users:
            if(u.userId==fromUser.userId): continue
            u.appendMessageToUser((key,messageData))
   
     
    def broadcastRawMessageToList(self,user,msg):
        for u in self.userBroadCastList:
            if(u.userId==user.userId): continue
            u.sendMessageToUser(dumps(msg))
   
     
    def broadCastMessageToUserList(self,user,key,msg):
        for u in self.userBroadCastList:
            if(u.userId==user.userId): continue
            u.sendMessageToUser(messageEncoder(key, msg))
            
    def broadCastMessageToMafiaList(self,user,msg):
        for u in self.getAllMafia():
            if(u.userId==user.userId): continue
            u.sendMessageToUser(dumps(msg))    
    
    def userKillVote(self, user1 , user2):
        #user1 -> [user1,user2,user3]
        if(not self.userKillVotesMap.get(user1.userId,None)):
            self.userKillVotesMap[user1.userId] = []
            
        self.userKillVotesMap[user1.userId].append(user2)
    
    def mafiaKillChoices(self,user , killList):
        self.mafiaKillChoiceList[user.userId] = killList
    
    def userKillChoice(self,user1 , user2):
        self.userKillChoiceList[user1.userId] = user2.userId
    
    def getAnAliveUser(self):
        self.users.shuffle()
        for user in self.users:
            if(user.lifeState==IS_ALIVE):
                return user
        return None
    
    def processUserMessages(self, userId , msg):
        user = self.getUserById(userId)
        # this is just a helper when sending it to other users to mention its from the other user
        for key,message in messageDecoder(msg):
            if(user.getLifeState() != IS_ALIVE and self.timelineState == DAY):
                #broadCast to dead list 
                return
             
            elif( user.getLifeState() != IS_ALIVE ): 
                return
             
            if(key==TEXT_COMMUNICATION ):
                if(self.currentState == VOTING_ACTIVE):
                    self.broadcastMessageToUserList(user,  messagEncodeWithUserId(TEXT_COMUNICATION_FROM,userId,message)) #msg will already have userId from whomits sent
                elif(self.currentState == MAFIA_ACTIVE):
                    self.broadCastMessageToMafiaList(user,  messagEncodeWithUserId(TEXT_COMUNICATION_FROM,userId,message)) #msg will already have userId from whomits sent
               
            elif(key == MAFIA_KILL and self.currentState == MAFIA_ACTIVE):
                if(user.isMafia()):
                    self.broadCastMessageToMafiaList(user,messagEncodeWithUserId(MAFIA_KILL,userId,message))
                    self.mafiaKillChoices(user , loads(message)) 
                    
                    
            elif(key==POLICE_IDENTIFY and self.currentState == POLICE_ACTIVE):
                if(user.isPolice() and  not self.hasPoliceIdentified ):
                    userIdToIdentify = message
                    user.sendMessageToUser(messagEncodeWithUserId(POLICE_IDENTIFY_RESULT, userIdToIdentify ,self.getUserById(userIdToIdentify).type))
                    self.hasPoliceIdentified = True
                    
            elif(key == MEDIC_SAVE and self.currentState == MEDIC_ACTIVE):
                if(user.isMedic() and not self.hasMedicSavedAnUser):
                    userIdToSave = message
                    self.getUserById(userIdToSave).setLifeState = IS_ALIVE
                    userKilledInThisRound = "" 
                    for user in self.users:
                        if(user.lifeState == IS_DEAD):
                            userKilledInThisRound = user.userId
                    self.broadCastMessageToListAppend(messagEncodeWithUserId(KILLED_BY_MAFIA,userKilledInThisRound,user.type))
    
            elif(key == KILL_USER_VOTE and self.currentState == VOTING_ACTIVE):
                userIdToKill = message
                self.userKillChoice(userId, userIdToKill)
                self.broadcastRawMessageToList(user, messagEncodeWithUserId(KILL_USER_VOTE , userId,message))
                
            
                    
        
    def onGameEnd(self):
        #dispose all andc lean 
        self.isGameValid = False
        return
    