from pymongo import MongoClient
from Config import *
import MafiaGame 
import User 
import lxml
import os.path
import re
import string
import time
import random
import tornado.auth
import smtplib
import torndb
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import unicodedata
from tornado.options import define, options
import yajl as json
from Config import *
from serverCode.Config import _WEB_PORT
import sockjs.tornado 

MONGO_SERVER_HOST = "localhost"
MONGO_SERVER_PORT = 27017

freeUserPool = []
userIdMap= {}
users=[]
gamesList = []
gamesMap={}


define("port", default= _WEB_PORT , help="run on the given port", type=int)

cookieSecret = "11oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo="

class mafiaGameServer(tornado.web.Application):
    def __init__(self):
        settings = dict(
            static_path=os.path.join(os.path.dirname(__file__), "html/"),
            cookie_secret=cookieSecret,
            login_url="/auth/login",
            autoescape=None,
        )
        static_path = dict(path=settings['static_path'], default_filename='index.html')
           
        handlers = [
            (r"/")
            (r"/([^/]+)*", tornado.web.StaticFileHandler,static_path)               
        ]
        ChatRouter = sockjs.tornado.SockJSRouter(gameConnection,GAME_DATA_URL_PATH )
        
        tornado.web.Application.__init__(self, handlers, **settings)
      


def isMafiaGameActive(sessionId):
    return False

def setMafiaGameState(state):
    # on end end the state 
    return



# db connection
client = MongoClient(MONGO_SERVER_HOST,MONGO_SERVER_PORT)
db = client['mafiaGame']
userData = db.userData
    
def getUserObjectFromServerDb(self,userId):
    if(userIdMap.get(userId,None)!=None):
        return userIdMap.get(userId,None)
        
    userObj = userData.find_one({"userId": userId})
    activeSession = None
    lifeState = IS_ALIVE 
    pendingMessages = []
    if(isMafiaGameActive(userObj["activeSession"])): # fill the user object with current active game he was playing ,he can't start a new game while registered for a new game
        activeSession = userObj["activeSession"]
        lifeState = userObj["lifeStatus"]
        pendingMessages = userObj["pendingMessages"]
        
    return User(userId,activeSession , pendingMessages,lifeState)
    
    
    
def isLoggedIn(func):
    def wrapperToCheckLoggedIn(self):
        if(self.isLoggedIn()):
            func(self)
        else:
            self.redirect(PLEASE_LOGIN_URL) 
    return wrapperToCheckLoggedIn
        


class gameConnection(sockjs.tornado.SockJSConnection):
    """Chat connection implementation"""
    # Class level variable

    def on_open(self, info):
        print info
        userId = self.get_secure_cookie("userId",None)
        if(not userId):
            self.close()
        
    def on_message(self, msg):
        # message is alway json
        msg = json.loads(msg)
        sessionId = msg["sessionId"]
        userIdEncoded = msg["userId"]
        userId = tornado.web.decode_signed_value(cookieSecret , "userId",userIdEncoded)
        self.dataRecieved(sessionId, userId, msg)     

    def on_close(self):
        # Remove client from the clients list and broadcast leave message
        pass 
        # destroy the user object and load it in db 
        
        
    def dataRecieved(self,sessionId,userId,msg):
        game = gamesMap.get(sessionId,None)
        if(not game):
            return
        game.processUserMessages(userId , msg)
        user = game.getUserById(userId)
        user.connection = self
        #user.setConnection()
            
class gameRegistration(tornado.web.RequestHandler):
    
    def prepare(self):
        pass
    
    def get(self,*args):
        self.post()
        
    def post(self,*args):
        if(args[0]==USER_DEVICE_ID_URL_PATH):# also need more param from local phone db to be secure
            deviceId =""
            userId = self.get_secure_cookie("userId",None)
            if(not userId and not self.getUserByDeviceId(deviceId)):# very new user
                #self.render(LOGIN_WITH_FB_URL) # only one time goes through fb login and reisters user 
                pass
            else:
                self.authorizeUser(userId, deviceId) # atleast one of it needed
            
        
        elif(args[0]==REGISTER_USER_URL_PATH):# from fb data
            userId =""
            userPic=""
            userName =""
            email=""
            user = self.saveUser(userId, userPic,userName,email)
            self.redirect(GAME_HOME_SCREEN_URL)
        
        elif(args[0]==CREATE_NEW_GAME_URL_PATH):# from fb data
            privateSession= True if self.get_argument("isPrivate",False)=='true' else False
            sessionId  = self.createNewGame(privateSession)
            self.write({"sessionId":sessionId})
            

            
            
    def getUserByDeviceId(self,deviceId):
        # return User object
        pass

    def authorizeUser(self,userId, deviceId):
        user = self.getUserByDeviceId(deviceId)
        if(not user):
            #TODO , save the user - deviceid map here 
            pass
        self.set_secure_cookie("userId",user.userId)
        
        
    def isLoggedIn(self):
        if(not self.get_secure_cookie("userId",None)):
            return False
        return True
            
    def saveuser(self,userId, userPic, userName , email ):
        pass
    

        
    
    @isLoggedIn
    def createNewGame(self,isPrivate=False):
        if(len(freeUserPool)< 6):
            return False
        
        usersToStartWith = freeUserPool[:16]
        newGame = MafiaGame()
        sessionId = newGame.getSessionId()
        gamesMap[sessionId]=newGame
        return sessionId
    
         
        
        
        
    
        